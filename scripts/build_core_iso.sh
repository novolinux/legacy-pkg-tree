#!/bin/sh

rm -rf ~/iso
mkdir -p ~/iso

sh mkimage.sh --tag 0.1 \
	--outdir ~/iso \
	--arch x86_64 \
	--repository http://novolinux.org/core \
	--profile core

