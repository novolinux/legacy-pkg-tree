#!/bin/sh

NATIVE_ARCH=$(uname -m)
TARGET_ARCH="$1"
SUDO_APK=abuild-apk

cd ../core

if [ -z "$TARGET_ARCH" ] || [ "$TARGET_ARCH" == "$NATIVE_ARCH" ]; then
	for PKG in *; do
	    cd "$PKG"
	    abuild -r
	    cd ..
	done
else
	[ -e /usr/share/abuild/functions.sh ] || (echo "abuild not found" ; exit 1)
	CBUILDROOT="$(CTARGET=$TARGET_ARCH . /usr/share/abuild/functions.sh ; echo $CBUILDROOT)"
	. /usr/share/abuild/functions.sh
	[ -z "$CBUILD_ARCH" ] && die "abuild is too old (use 2.29.0 or later)"
	[ -z "$CBUILDROOT" ] && die "CBUILDROOT not set for $TARGET_ARCH"

	[ -z "$APORTS" ] && APORTS=$(realpath $(dirname $0)/../)
	[ -e "$APORTS/core/build-base" ] || die "Unable to deduce aports base checkout"

	apkbuildname() {
		local repo="${1%%/*}"
		local pkg="${1##*/}"
		[ "$repo" = "$1" ] && repo="core"
		echo $APORTS/$repo/$pkg/APKBUILD
	}

	msg() {
		[ -n "$quiet" ] && return 0
		local prompt="$GREEN>>>${NORMAL}"
		local name="${BLUE}bootstrap-${TARGET_ARCH}${NORMAL}"
	        printf "${prompt} ${name}: %s\n" "$1" >&2
	}

	if [ ! -d "$CBUILDROOT" ]; then
		msg "Creating sysroot in $CBUILDROOT"
		mkdir -p "$CBUILDROOT/etc/apk/keys"
		cp -a /etc/apk/keys/* "$CBUILDROOT/etc/apk/keys"
		${SUDO_APK} add --quiet --initdb --arch $TARGET_ARCH --root $CBUILDROOT
	fi

	msg "Building cross-compiler"

	CTARGET=$TARGET_ARCH BOOTSTRAP=nobase APKBUILD=$(apkbuildname binutils) abuild -r

	if ! CHOST=$TARGET_ARCH BOOTSTRAP=nolibc APKBUILD=$(apkbuildname musl) abuild up2date 2>/dev/null; then
		CHOST=$TARGET_ARCH BOOTSTRAP=nocc APKBUILD=$(apkbuildname musl) abuild -r

		EXTRADEPENDS_HOST="musl-dev" \
		CTARGET=$TARGET_ARCH BOOTSTRAP=nolibc APKBUILD=$(apkbuildname gcc) abuild -r

		EXTRADEPENDS_BUILD="gcc-pass2-$TARGET_ARCH" \
		CHOST=$TARGET_ARCH BOOTSTRAP=nolibc APKBUILD=$(apkbuildname musl) abuild -r
	fi

	EXTRADEPENDS_TARGET="musl musl-dev" \
	CTARGET=$TARGET_ARCH BOOTSTRAP=nobase APKBUILD=$(apkbuildname gcc) abuild -r

	CTARGET=$TARGET_ARCH BOOTSTRAP=nobase APKBUILD=$(apkbuildname build-base) abuild -r

	msg "Cross building base system"

	apk info --quiet --installed --root "$CBUILDROOT" libgcc libstdc++ musl-dev || \
		${SUDO_APK} --root "$CBUILDROOT" add --repository "$REPODEST/core" libgcc libstdc++ musl-dev

	for PKG in *; do
		CHOST=$TARGET_ARCH BOOTSTRAP=bootimage APKBUILD=$(apkbuildname $PKG) abuild -r

		case "$PKG" in
		fortify-headers | libc-dev | build-base)
			apk info --quiet --installed --root "$CBUILDROOT" $PKG || \
				${SUDO_APK} --update --root "$CBUILDROOT" --repository "$REPODEST/core" add $PKG
			;;
		musl | gcc)
			[ "$(apk upgrade --root "$CBUILDROOT" --repository "$REPODEST/core" --available --simulate | wc -l)" -gt 1 ] &&
				${SUDO_APK} upgrade --root "$CBUILDROOT" --repository "$REPODEST/core" --available
			;;
		esac
	done
fi

cd ../scripts
