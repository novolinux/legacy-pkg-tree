FROM alpine:edge

RUN apk -U add alpine-sdk build-base apk-tools alpine-conf busybox fakeroot syslinux xorriso mtools dosfstools grub-efi shadow zip rsync
RUN useradd -u 1111 -m build -G abuild

COPY --chown=build ./ /home/build/tree

USER build
WORKDIR /home/build/tree/scripts

ENTRYPOINT ["./build_core_arm.sh"]
CMD [""]
