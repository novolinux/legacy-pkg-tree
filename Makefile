.PHONY: clean build test

all: build

clean:
	@rm -rf *.gz *.iso

build:
	@docker build -t projecttux/builder .
	@docker run -i -t --name projecttux_build -v ~/.abuild:/home/build/.abuild projecttux/builder
	@docker rm -f projecttux_build